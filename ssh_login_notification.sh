#!/bin/sh

if ! [ $(id -u) = 0 ]; then
   echo "I am not root!"
   exit 1
fi

if readlink /proc/$$/exe | grep -q "dash"; then
	echo 'This installer needs to be run with "bash", not "sh".'
	exit
fi

# Detect OS
# $os_version variables aren't always in use, but are kept here for convenience
if grep -qs "ubuntu" /etc/os-release; then
	os="ubuntu"
	group_name="nogroup"
elif [[ -e /etc/debian_version ]]; then
	os="debian"
	group_name="nogroup"
elif [[ -e /etc/centos-release ]]; then
	os="centos"
	group_name="nobody"
elif [[ -e /etc/fedora-release ]]; then
	os="fedora"
	group_name="nobody"
else
	echo "This installer seems to be running on an unsupported distribution.
Supported distributions are Ubuntu, Debian, CentOS, and Fedora."
	exit
fi

echo "Введите TOKEN (telegram bot):"
read TOKEN
echo "Введите CHAT ID:"
read ID

if [[ "$os" = "debian" || "$os" = "ubuntu" ]]; then
	apt-get update
	apt-get install -y curl
elif [[ "$os" = "centos" ]]; then
	yum update
	yum install -y curl
else
	# Else, OS must be Fedora
        yum update
        yum install -y curl
fi


#cp /root/ssh_login_notification/Files/onlogged /sbin/ && chmod +x /sbin/onlogged
cp ./Files/onlogged /sbin/ && chmod +x /sbin/onlogged
cat ./Files/telegram | sed "s/{TOKEN}/$TOKEN/g" | sed "s/{CHAT_ID}/$ID/g" > /sbin/telegram
chmod +x /sbin/telegram
echo "/sbin/onlogged ssh" > /etc/ssh/sshrc
echo "/sbin/onlogged bash" >> /etc/bash.bashrc
