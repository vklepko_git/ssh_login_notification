# Уведомления о входах на сервер (SSH/TERMINAL)

Для начала нужно:
1. Cоздать бота через "@BotFather" и получить его TOKEN
2. Узнать свой  CHAT_ID или группы через "@myidbot" 


## Установка на сервер

Заходим на сервер и переходим в директорию **/root** под пользователем **root**.

Устанавливаем Git:

```sh
apt update
apt install -y git
```

Теперь скачиваем репозиторий со скриптом:

```sh
git clone https://gitlab.com/vklepko_git/ssh_login_notification.git
```
Переходим в скачанный каталог и запускаем скрипт: 

```sh
cd ssh_login_notification
bash ssh_login_notification.sh
```
